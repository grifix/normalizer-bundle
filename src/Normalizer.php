<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle;

use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Grifix\Normalizer\Normalizer as BaseNormalizer;
use Grifix\Normalizer\ObjectNormalizers\CustomNormalizerWrapper\CustomNormalizerWrapperFactory;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizerFactory;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\SchemaValidator\Repository\SchemaRepositoryInterface;
use Grifix\Normalizer\VersionConverter\Repository\VersionConverterRepositoryInterface;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class Normalizer extends BaseNormalizer
{
    public function __construct(
        NormalizerRepositoryInterface $normalizerRepository,
        DefaultObjectNormalizerFactory $defaultObjectNormalizerFactory,
        CustomNormalizerWrapperFactory $customNormalizerWrapperFactory,
        SchemaRepositoryInterface $schemaRepository,
        VersionConverterRepositoryInterface $versionConverterRepository,
        DependencyProviderInterface $dependencyProvider,
        array $configs
    ) {
        parent::__construct(
            $normalizerRepository,
            $defaultObjectNormalizerFactory,
            $customNormalizerWrapperFactory,
            $schemaRepository,
            $versionConverterRepository,
            $dependencyProvider
        );
        $this->applyConfigs($configs, $dependencyProvider);
    }

    private function applyConfigs(array $configs, DependencyProviderInterface $dependencyProvider): void
    {
        foreach ($configs as $config) {
            $versionConverter = null;
            if (isset($config['version_converter'])) {
                /** @var VersionConverterInterface $versionConverter */
                $versionConverter = $dependencyProvider->get($config['version_converter']);
            }
            if (isset($config['object_class'])) {
                $this->registerDefaultObjectNormalizer(
                    $config['name'],
                    $config['object_class'],
                    $this->createSchemas($config['schemas']),
                    $versionConverter,
                    $config['dependencies'] ?? []
                );
                continue;
            }
            if (isset($config['class'])) {
                /** @var CustomObjectNormalizerInterface $normalizer */
                $normalizer = $dependencyProvider->get($config['class']);
                $this->registerCustomObjectNormalizer(
                    $config['name'],
                    $normalizer,
                    $this->createSchemas($config['schemas']),
                    $versionConverter,
                );
                continue;
            }
            throw new \Exception('Invalid normalizer configuration!');
        }
    }

    /**
     * @return Schema[]
     */
    private function createSchemas(array $schemaConfigs): array
    {
        $result = [];
        foreach ($schemaConfigs as $schemaConfig) {
            $schema = Schema::create();
            foreach ($schemaConfig as $propertyConfig) {
                $this->addPropertyToSchema($schema, $propertyConfig);
            }
            $result[] = $schema;
        }
        return $result;
    }

    private function addPropertyToSchema(Schema $schema, array $propertyConfig): void
    {
        $this->validateSchemaPropertyConfig($propertyConfig);
        match ($propertyConfig['type']) {
            'object' => $schema->withObjectProperty(
                $propertyConfig['property'],
                $propertyConfig['allowed_normalizers'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'array_of_objects' => $schema->withArrayOfObjectsProperty(
                $propertyConfig['property'],
                $propertyConfig['allowed_normalizers'],
            ),
            'string' => $schema->withStringProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'integer' => $schema->withIntegerProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'number' => $schema->withNumberProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'array' => $schema->withArrayProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'boolean' => $schema->withBooleanProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            ),
            'mixed_object' => $schema->withMixedObjectProperty(
                $propertyConfig['property'],
                (bool)($propertyConfig['nullable'] ?? false)
            )
        };
    }

    private function validateSchemaPropertyConfig(array $propertyConfig): void
    {
        if (!isset($propertyConfig['type'])) {
            throw new \RuntimeException('Schema property type is not defined!');
        }
        if (!isset($propertyConfig['property'])) {
            throw new \RuntimeException('Schema property name is not defined!');
        }
        if (!in_array(
            $propertyConfig['type'],
            [
                'object',
                'array_of_objects',
                'mixed_object',
                'string',
                'integer',
                'number',
                'array',
                'boolean'
            ]
        )
        ) {
            throw new \RuntimeException(sprintf('Invalid schema property type [%s]!', $propertyConfig['type']));
        }

        if ($propertyConfig['type'] === 'object' && !isset($propertyConfig['allowed_normalizers'])) {
            throw new \RuntimeException('Allowed normalizers must be defined for the property with [object] type!');
        }

        if ($propertyConfig['type'] === 'array_of_objects' && !isset($propertyConfig['allowed_normalizers'])) {
            throw new \RuntimeException('Allowed normalizers must be defined for the property with [array_of_objects] type!');
        }
    }
}
