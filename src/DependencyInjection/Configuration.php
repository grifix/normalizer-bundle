<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_normalizer');

        /** @formatter:off */
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('normalizers')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->cannotBeEmpty()->end()
                            ->scalarNode('object_class')->cannotBeEmpty()->end()
                            ->scalarNode('class')->cannotBeEmpty()->end()
                            ->scalarNode('version_converter')->defaultNull()->end()
                            ->arrayNode('dependencies')
                                ->scalarPrototype()->end()
                           ->end()
                           ->arrayNode('schemas')->cannotBeEmpty()
                                ->arrayPrototype()
                                    ->arrayPrototype()
                                        ->children()
                                            ->scalarNode('property')->cannotBeEmpty()->end()
                                            ->scalarNode('type')->cannotBeEmpty()->end()
                                            ->arrayNode('allowed_normalizers')->cannotBeEmpty()
                                                ->scalarPrototype()->end()
                                            ->end()
                                            ->scalarNode('nullable')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
        ;
        /** @formatter:on */

        return $treeBuilder;
    }
}
