<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle;

use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class DependencyProvider implements DependencyProviderInterface
{

    public function __construct(private readonly ContainerInterface $container)
    {
    }

    public function get(string $class): object
    {
        return $this->container->get($class);
    }

    public function set(object $object): void
    {
        throw new \Exception('Not implemented!');
    }
}
