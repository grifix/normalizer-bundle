<?php
declare(strict_types=1);

namespace Grifix\NormalizerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixNormalizerBundle extends Bundle
{
}

