Integration [grifix/normalizer](https://packagist.org/packages/grifix/normalizer) with [Symfony](https://symfony.com/)

# Installation
`composer require grifix/normalizer-bundle`

# Usage
- Read the [Grifix Normalizer documentation](https://gitlab.com/grifix/normalizer/-/blob/3.0/README.md)
- Inject normalizer as a dependency or get it from 
the [Symfony Container](https://symfony.com/doc/current/service_container.html) 
instead of creating it by `Normalizer::create()`
- use `packages/grifix_normalizer.yaml` instead of methods:
    - `registerCustomObjectNormalizer`
    - `registerDefaultObjectNormalizer`
- instead of `registerDependency` method usage, register dependency by
[Symfony Container](https://symfony.com/doc/current/service_container.html)

# Default normalizer configuration example:
Open `packages/grifix_normalizer.yaml` and add the following configuration to the `grifix_normalizer.normalizers` section:
```yaml
   grifix_normalizer:
     normalizers:
       #date
       - name: date
         class: Grifix\NormalizerBundle\Tests\Dummies\DateNormalizer
         schemas:
           - - property: value
               type: string
       #entity
       - name: entity
         version_converter: Grifix\NormalizerBundle\Tests\Dummies\EntityVersionConverter
         object_class: Grifix\NormalizerBundle\Tests\Dummies\Entity
         dependencies:
           - service
         schemas:
           #version 1
           - - property: name
               type: string
             - property: dates
               type: array_of_objects
               allowed_normalizers: [date]
             - property: public
               type: boolean
             - property: number
               type: integer
             - property: price
               type: number
             - property: notes
               type: array
             - property: mixed
               type: mixed_object
           #version 2
           - - property: name
               type: string
             - property: dates
               type: array_of_objects
               allowed_normalizers: [date]
             - property: public
               type: boolean
             - property: number
               type: integer
             - property: price
               type: number
             - property: notes
               type: array
             - property: mixed
               type: mixed_object
             - property: date
               type: object
               allowed_normalizers: [date]
               nullable: true
       #vo
       - name: vo
         object_class: Grifix\NormalizerBundle\Tests\Dummies\Vo
         schemas:
           - - property: value
               type: string
```
 - register `Grifix\NormalizerBundle\Tests\Dummies\EntityVersionConverter` as a public service
 - register `Entity` dependencies as public services


# Custom normalizer configuration example:
Open `packages/grifix_normalizer.yaml` and add the following configuration to the `grifix_normalizer.normalizers` section:
```yaml
  #date
  - name: date
    class: Grifix\NormalizerBundle\Tests\Dummies\DateNormalizer
    schemas:
      - - property: value
          type: string
```
