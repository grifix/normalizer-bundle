<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests\Dummies;

final class Entity
{

    /**
     * @param Date[] $dates
     * @param string[] $notes
     */
    public function __construct(
        public readonly string $name,
        public readonly Service $service,
        public readonly array $dates,
        public readonly bool $public,
        public readonly int $number,
        public readonly ?float $price,
        public readonly array $notes,
        public readonly object $mixed,
        public readonly ?Date $date = null,
    ) {
    }
}
