<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests\Dummies;

final class Date
{
    private \DateTimeImmutable $value;

    public function __construct(string $value)
    {
        $this->value = new \DateTimeImmutable($value);
    }

    public function toString(): string
    {
        return $this->value->format(DATE_ATOM);
    }
}
