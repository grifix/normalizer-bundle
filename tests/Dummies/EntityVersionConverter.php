<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests\Dummies;

use Grifix\Normalizer\VersionConverter\Exceptions\UnsupportedVersionException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class EntityVersionConverter implements VersionConverterInterface
{

    public function convert(array $data, int $dataVersion, string $normalizerName): array
    {
        return match ($dataVersion) {
            1 => $this->convertToVersion2($data),
            default => throw new UnsupportedVersionException(
                $normalizerName,
                $dataVersion
            )
        };
    }

    public function convertToVersion2(array $data): array
    {
        $data['date'] = null;

        return $data;
    }
}
