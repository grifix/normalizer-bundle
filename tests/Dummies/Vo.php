<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests\Dummies;

final class Vo
{
    public function __construct(public readonly string $value)
    {
    }
}
