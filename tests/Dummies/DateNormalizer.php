<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests\Dummies;

use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;

final class DateNormalizer implements CustomObjectNormalizerInterface
{

    public function normalize(object $object): array
    {
        return [
            'value' => $object->toString()
        ];
    }

    public function denormalize(array $data): object
    {
        return new Date($data['value']);
    }

    public function getObjectClass(): string
    {
        return Date::class;
    }
}
