<?php

declare(strict_types=1);

namespace Grifix\NormalizerBundle\Tests;

use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Grifix\NormalizerBundle\Tests\Dummies\Date;
use Grifix\NormalizerBundle\Tests\Dummies\Entity;
use Grifix\NormalizerBundle\Tests\Dummies\Service;
use Grifix\NormalizerBundle\Tests\Dummies\Vo;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class NormalizerBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixNormalizerBundle::class);
        $kernel->addTestConfig(__DIR__ . '/config.yaml');
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testInitBundle(): void
    {
        $container = self::getContainer();

        $this->assertTrue($container->has(NormalizerInterface::class));
        $service = $container->get(NormalizerInterface::class);
        $this->assertInstanceOf(Normalizer::class, $service);
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();

        $container = $kernel->getContainer();
        /** @var NormalizerInterface $normalizer */
        $normalizer = $kernel->getContainer()->get(NormalizerInterface::class);
        /** @var Service $service */
        $service = $container->get(Service::class);
        $entity = new Entity(
            'Apple',
            $service,
            [
                new Date('2001-01-01')
            ],
            true,
            20,
            12.45,
            [
                'one',
                'two'
            ],
            new Date('2001-01-01'),
            null
        );
        $data = $normalizer->normalize($entity);
        self::assertEquals($entity, $normalizer->denormalize($data));

        $entity = new Entity(
            'Apple',
            $service,
            [],
            false,
            20,
            12.45,
            [],
            new Date('2001-01-01'),
            new Date('2001-01-01')
        );
        $data = $normalizer->normalize($entity);
        self::assertEquals($entity, $normalizer->denormalize($data));

        self::assertEquals(
            new Entity(
                'Apple',
                $service,
                [
                    new Date('2001-01-01')
                ],
                true,
                20,
                12.45,
                [
                    'one',
                    'two'
                ],
                new Vo('test'),
            ),
            $normalizer->denormalize(
                [
                    'name' => 'Apple',
                    'dates' =>
                        [
                            0 =>
                                [
                                    'value' => '2001-01-01T00:00:00+00:00',
                                    '__normalizer__' =>
                                        [
                                            'name' => 'date',
                                            'version' => 1,
                                        ],
                                ],
                        ],
                    'public' => true,
                    'number' => 20,
                    'price' => 12.45,
                    'notes' =>
                        [
                            0 => 'one',
                            1 => 'two',
                        ],
                    'mixed' => [
                        'value' => 'test',
                        '__normalizer__' =>
                            [
                                'name' => 'vo',
                                'version' => 1
                            ]
                    ],
                    '__normalizer__' =>
                        [
                            'name' => 'entity',
                            'version' => 1,
                        ],
                ]
            )
        );

        self::assertEquals(
            new Entity(
                'Apple',
                $service,
                [
                    new Date('2001-01-01')
                ],
                true,
                20,
                12.45,
                [
                    'one',
                    'two'
                ],
                new Vo('test'),
                new Date('2001-01-01')
            ),
            $normalizer->denormalize(
                [
                    'name' => 'Apple',
                    'dates' =>
                        [
                            0 =>
                                [
                                    'value' => '2001-01-01T00:00:00+00:00',
                                    '__normalizer__' =>
                                        [
                                            'name' => 'date',
                                            'version' => 1,
                                        ],
                                ],
                        ],
                    'public' => true,
                    'number' => 20,
                    'price' => 12.45,
                    'mixed' => [
                        'value' => 'test',
                        '__normalizer__' =>
                            [
                                'name' => 'vo',
                                'version' => 1
                            ]
                    ],
                    'date' => [
                        'value' => '2001-01-01T00:00:00+00:00',
                        '__normalizer__' =>
                            [
                                'name' => 'date',
                                'version' => 1,
                            ],
                    ],
                    'notes' =>
                        [
                            0 => 'one',
                            1 => 'two',
                        ],
                    '__normalizer__' =>
                        [
                            'name' => 'entity',
                            'version' => 2,
                        ],
                ]
            )
        );
    }
}
